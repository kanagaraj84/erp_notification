<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectuploadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          Schema::create('projectuploads', function (Blueprint $table) {
            $table->increments('id');            
            $table->integer('project_id')->unsigned();
            $table->integer('user_id')->unsigned() ;
            $table->integer('group_id')->unsigned();  
            $table->string('uploaded_files');           
            $table->timestamps();
        });
        
        /*
        Delete tasks associated with this project ID
        */
        Schema::table('projectuploads', function (Blueprint $table) {
            $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade') ;
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade') ;
            $table->foreign('group_id')->references('id')->on('groups')->onDelete('cascade') ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('projectupload');
    }
}
