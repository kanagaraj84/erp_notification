<?php

Auth::routes();


Route::get('/', function () {
	// return view('welcome');
	return redirect('/login') ;
});

Route::get('/main-search-autocomplete', function(){
    return json_encode(DB::table('tasks')->get()->all() );
});

Route::get('post', 'PostController@create')->name('create');
Route::post('post', 'PostController@store')->name('store');

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function(){


	// ===================== PROJECTS ======================

	Route::get('/projects/uploadform', 'ProjectController@uploadform')->name('project.uploadform') ;
    Route::post('/projects/fupload', 'ProjectController@fupload')->name('project.fupload') ;
    
	Route::get('/projects', 'ProjectController@index')->name('project.show') ;

	Route::get('/projects/create', 'ProjectController@create')->name('project.create') ;

	Route::get('/projects/edit/{id}', 'ProjectController@edit')->name('project.edit') ;

	Route::post('/projects/update/{id}', 'ProjectController@update')->name('project.update') ;

	Route::get('/projects/delete/{id}', 'ProjectController@destroy')->name('project.delete') ;

	// Store the new project from the form posted with the view Above
	Route::post('/projects/store', 'ProjectController@store')->name('project.store');

	// =====================GROUPS ======================
	Route::get('/groups', 'GroupController@index')->name('group.show') ;

	Route::get('/groups/create', 'GroupController@create')->name('group.create') ;

	Route::get('/groups/edit/{id}', 'GroupController@edit')->name('group.edit') ;

	Route::post('/groups/update/{id}', 'GroupController@update')->name('group.update') ;

	Route::get('/groups/delete/{id}', 'GroupController@destroy')->name('group.delete') ;

	// Store the new project from the form posted with the view Above
	Route::post('/groups/store', 'GroupController@store')->name('group.store');

    // =====================ROLES ======================
    Route::get('/roles', 'RoleController@index')->name('role.show') ;

    Route::get('/roles/create', 'RoleController@create')->name('role.create') ;

    Route::get('/roles/edit/{id}', 'RoleController@edit')->name('role.edit') ;

    Route::post('/roles/update/{id}', 'RoleController@update')->name('role.update') ;

    Route::get('/roles/delete/{id}', 'RoleController@destroy')->name('role.delete') ;

    // Store the new project from the form posted with the view Above
    Route::post('/roles/store', 'RoleController@store')->name('role.store');

	// ====================  TASKS =======================
	Route::get('/tasks','TaskController@index')->name('task.show') ;

	Route::get('/tasks/view/{id}','TaskController@view')->name('task.view') ;

	// Display the Create Task View form
	Route::get('/tasks/create', 'TaskController@create')->name('task.create'); 

	// Store the new task from the form posted with the view Above
	Route::post('/tasks/store', 'TaskController@store')->name('task.store');

	// Search view
	// Route::get('/tasks/search', 'TaskController@searchTask')->name('task.search');
    // USER TASK SEARCH
    Route::get('tasks/search', 'TaskController@searchTask')->name('task.search') ;

	// Sort Table
	Route::get('/tasks/sort/{key}', 'TaskController@sort')->name('task.sort') ;

	Route::get('/tasks/edit/{id}','TaskController@edit')->name('task.edit');

	Route::get('/tasks/list/{projectid}','TaskController@tasklist')->name('task.list');
	Route::get('/tasks/delete/{id}', 'TaskController@destroy')->name('task.delete') ;
	Route::get('/tasks/deletefile/{id}', 'TaskController@deleteFile')->name('task.deletefile') ;
	Route::post('/tasks/update/{id}', 'TaskController@update')->name('task.update') ;
	Route::get('/tasks/completed/{id}','TaskController@completed')->name('task.completed');

	// =====================  USERS   ============================
	Route::get('/users', 'UserController@index')->name('user.index'); 
	Route::get('/users/list/{id}','UserController@userTaskList')->name('user.list');
	Route::get('/users/create', 'UserController@create')->name('user.create'); 
    Route::post('/users/store', 'UserController@store')->name('user.store'); 
	Route::get('/users/edit/{id}', 'UserController@edit')->name('user.edit'); 
	Route::post('/users/update/{id}', 'UserController@update')->name('user.update') ;
    Route::get('/users/activate/{id}', 'UserController@activate')->name('user.activate') ; 
    Route::get('/users/delete/{id}', 'UserController@destroy')->name('user.delete') ;
    Route::get('/users/disable/{id}', 'UserController@disable')->name('user.disable') ;

});










