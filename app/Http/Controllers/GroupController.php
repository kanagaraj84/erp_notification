<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use App\Group;
use App\User;


class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */ 
    public function index()
    {
        // $tasks = Task::
        $groups = Group::all() ;
        return view('group.groups')->with('groups', $groups) ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $proje
        return view('group.create') ;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $groups_count = Group::count() ;
      
        if ( $groups_count < 10  ) {  
            
            // dd( $request->all()  ) ;
            $this->validate( $request, [
                'group' => 'required'
            ] ) ;        
    
            $group_new = new Group;
            $group_new->group_name = $request->group;
            $group_new->save() ;
            Session::flash('success', 'Group Created') ;
            return redirect()->route('group.show') ;
        }
        
        else {
            Session::flash('info', 'Please delete some groups, Demo max: 10') ;
            return redirect()->route('group.show') ;          
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit_group =  Group::find($id) ;
        return view('group.edit')->with('edit_group', $edit_group);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update_group = Group::find($id) ;
        $update_group->group_name = $request->name;
        $update_group->save() ;
        Session::flash('success', 'Group was sucessfully edited') ;
        return redirect()->route('group.show') ;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete_group = Group::find($id) ;
        $delete_group->delete() ;
        Session::flash('success', 'Group was deleted') ;
        return redirect()->back();        
        
    }

}
