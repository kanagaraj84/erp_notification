<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
class PostController extends Controller
{
    //
	
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
	
	public function create()
	{
		return view('post');
	}	
	
	public function store(Request $request)
	{
		 $post = new Post;
		 $post->title = $request->get('title');
		 $post->body = $request->get('body');
		 $post->save();
		 return response($post);
	}


}
