<?php
namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use App\Project;
use App\Task;
use App\Projectupload;
use App\Group;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use ZipArchive;
class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */ 
    public function index()
    {
        // $tasks = Task::
        $projects = Project::all() ;
           $groups = Group::all();
        return view('project.projects')->with('projects', $projects)->with('groups', $groups);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $proje
        return view('project.create') ;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $projects_count = Project::count() ;
      
        if ( $projects_count < 10  ) {  
            
            // dd( $request->all()  ) ;
            $this->validate( $request, [
                'project' => 'required'
            
            ] ) ;        
    
            $project_new = new Project;
            $project_new->project_name = $request->project;
            $project_new->group_id   = $request->group_id;
            $project_new->save() ;
            Session::flash('success', 'Project Created') ;
            return redirectroute('project.show') ;
        }
        
        else {
            Session::flash('info', 'Please delete some projects, Demo max: 10') ;
            return redirect()->route('project.show') ;          
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit_project =  Project::find($id) ;
        return view('project.edit')->with('edit_project', $edit_project)  ;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update_project = Project::find($id) ;
        $update_project->project_name = $request->name;
        $update_project->save() ;
        Session::flash('success', 'Project was sucessfully edited') ;
        return redirect()->route('project.show') ;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete_project = Project::find($id) ;
        $delete_project->delete() ;
        Session::flash('success', 'Project was deleted and tasks associated with it') ;
        return redirect()->back();        
        
    }

     public function uploadform()
    {
        $fileuploads = Projectupload::all();
        $users =  User::all() ;
        $projects = Project::all() ;
        $groups =  Group::find(Auth::user()->group_id) ;
        $groupname = isset($groups) ? $groups->group_name : '';
        return view('project.uploadform')->with('fileuploads', $fileuploads)->with('users', $users)->with('projects', $projects)->with('groupname', $groupname);
    }

    public function fupload(Request $request)
    {

        $file = Input::file('uploaded_files');
        $group_name = $request->group_name;
        $destinationPath = public_path(). '/uploads/'.$group_name."/";
        $filename = $file->getClientOriginalName();

        Input::file('uploaded_files')->move($destinationPath, $filename);

        Projectupload::create([
            'project_id' => $request->project_id,
            'user_id'    => $request->user_id,
            'group_id' => $request->group_id,
            'uploaded_files' => $filename
        ]);

        Session::flash('success', 'Project Uploaded') ;
        return redirect()->route('project.uploadform') ;

    }
}