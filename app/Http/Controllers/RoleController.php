<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use App\Role;
use App\User;


class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */ 
    public function index()
    {
        // $tasks = Task::
        $roles = Role::all() ;
        return view('role.roles')->with('roles', $roles) ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('role.create') ;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $roles_count = Role::count() ;
      
        if ( $roles_count < 10  ) {
            
            // dd( $request->all()  ) ;
            $this->validate( $request, [
                'role' => 'required'
            ] ) ;        
    
            $role_new = new Role;
            $role_new->role_name = $request->role;
            $role_new->save() ;
            Session::flash('success', 'Role Created') ;
            return redirect()->route('role.show') ;
        }
        
        else {
            Session::flash('info', 'Please delete some roles, Demo max: 10') ;
            return redirect()->route('role.show') ;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit_role =  Role::find($id) ;
        return view('role.edit')->with('edit_role', $edit_role);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update_role = Role::find($id) ;
        $update_role->role_name = $request->name;
        $update_role->save() ;
        Session::flash('success', 'Role was sucessfully edited') ;
        return redirect()->route('role.index') ;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete_role = Role::find($id) ;
        $delete_role->delete() ;
        Session::flash('success', 'Role was deleted') ;
        return redirect()->back();
        
    }

}
