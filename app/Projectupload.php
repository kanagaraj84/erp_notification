<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Projectupload extends Model
{
    protected $fillable = [ 
    	'project_id','user_id', 'group_id', 'uploaded_files'
     ] ;

     
     public function project() {

     	return $this->belongsTo('App\Project') ;
     }

     public function user() {

         return $this->belongsTo('App\User') ;
     }

     public function group() {

         return $this->hasMany('App\Group') ;
     }

}
