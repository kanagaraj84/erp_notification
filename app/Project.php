<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    //
    protected $fillable = [ 
    	'project_name','group_id'
    ] ;


    public function tasks() {
    	return $this->hasMany('App\Task');
    }


}
