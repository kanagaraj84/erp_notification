@extends('layout')

@section('content')

@include('includes.errors') 

<form id="role_form" action="{{ route('role.update', [ 'id' => $edit_role->id ]) }}" method="POST">
    {{ csrf_field() }}

<label>Edit Role<span class="glyphicon glyphicon-edit" aria-hidden="true"></span></label>

<div class="row">
    <div class="col-md-8">
		<div class="form-group">
			<input type="text" class="form-control" id="role" name="name" value="{{ $edit_role->role_name }}">
		</div>
	</div>


	<div class="col-md-4">
		<div class="btn-group">
			<input class="btn btn-primary" type="submit" value="Submit" onclick="return validateForm()">
			<a class="btn btn-default" href="{{ redirect()->getUrlGenerator()->previous() }}">Cancel</a>
		</div>
	</div>
</div>

</form>

@stop


