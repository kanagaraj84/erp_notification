@extends('layout')

@section('content')

@include('includes.errors') 

<form id="group_form" action="{{ route('role.store') }}" method="POST">
    {{ csrf_field() }}

<label>Create a new Role<span class="glyphicon glyphicon-plus" aria-hidden="true"></span></label>

<div class="row">
    <div class="col-md-8">
		<div class="form-group">
			<input type="text" class="form-control" id="role" name="role">
		</div>
	</div>


	<div class="col-md-4">
		<div class="btn-group">
			<input class="btn btn-primary" type="submit" value="Submit" onclick="return validateForm()">
			<a class="btn btn-default" href="{{ redirect()->getUrlGenerator()->previous() }}">Cancel</a>
		</div>
	</div>
</div>

</form>

@stop

<script>
function validateForm() {
	console.log("VALIDATE FORM CLICKED") ;
	var group = document.forms["role_form"]["role"].value;

	if ( !group.length ) {
		swal("Enter Role Name", "" , "warning") ;
		document.getElementById("Role").focus();
		return false;
	}
}

</script>

