@extends('layout')

@section('content')


<form action="{{ route('user.update', [ 'id' => $user->id ] ) }}" method="POST">
    {{ csrf_field() }}


    <div class="col-md-8">

    	<div class="form-group">
    		<label>Edit Name</label>
			<input type="text" class="form-control"  name="name" value="{{ $user->name }}">
		</div>

    	<div class="form-group">
    		<label>Edit Email</label>
			<input type="text" class="form-control"  name="email" value="{{ $user->email }}">
		</div>

		<div class="form-group">
			<input type="text" class="form-control" placeholder="Update User Password (optional)" name="password">
		</div>

	</div>

	<div class="col-md-4">

		<div class="form-group">
			<label>Edit Role <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span></label>
			<select name="role" class="form-control">
				@if( $user->role == 'top_mgmt' )
			  		<option value="top_mgmt" selected>Top Management</option>
			  		<option value="mgr">Manager</option>
					<option value="tmember">Team Member</option>
			  	@elseif( $user->role == 'mgr' )
					<option value="top_mgmt">Top Management</option>
					<option value="mgr" selected>Manager</option>
					<option value="tmember">Team Member</option>
				@elseif( $user->role == 'tmember' )
					<option value="top_mgmt">Top Management</option>
					<option value="mgr">Manager</option>
					<option value="tmember" selected>Team Member</option>
			  	@endif
			</select>
		</div>
    <div class="form-group">
                        <label>Select Group<span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span></label>
                        @if ( !$groups->isEmpty() ) 

                        <select name="group_id" class="form-control">
                            @foreach ( $groups as $group)
                            <option value="{{ $group->id }}" selected>{{ $group->group_name }}</option>
                               @endforeach
                               @endif
                        </select>
                    </div>
		<div class="btn-group">
			<input class="btn btn-primary" type="submit" value="Submit">
			<a class="btn btn-default" href="{{ redirect()->getUrlGenerator()->previous() }}">Go Back</a>
		</div>

	</div>




</form>

@stop

