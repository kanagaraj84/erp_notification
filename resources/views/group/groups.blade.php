@extends('layout')

@section('content')


<h1>LIST OF ACTIVE Groups</h1>
     @if (Auth::user()->role=="superadmin" || Auth::user()->role=="mgr")  
<div class="new_project">
  <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>&nbsp;Add New Group</button>
</div>
@endif
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Enter Group Title</h4>
      </div>
      <div class="modal-body">
        <form id="group_form" action="{{ route('group.store') }}" method="POST">
            {{ csrf_field() }}

        <div class="row">
            <div class="col-md-12">
            <div class="form-group">
              <input type="text" class="form-control" id="group" name="group">
            </div>
          </div>

        </div>

        <div class="modal-footer">
          <input class="btn btn-primary" type="submit" value="Submit" >
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>

        </form>
      </div>

    </div>

  </div>
</div>
<!--  END modal  -->



<div class="table-responsive">
<table class="table table-striped">
    <thead>
      <tr>
        <th>Group ID</th>
        <th>Group Name</th>
             @if (Auth::user()->role=="superadmin")  
        <th>Actions</th>@endif
      </tr>
    </thead>

@if ( !$groups->isEmpty() ) 
    <tbody>
    @foreach ( $groups  as $group)
      <tr>
        <td>{{ $group->id}} </td>
        <td>
{{ $group->group_name }}
        </td>
             @if (Auth::user()->role=="superadmin")  
        <td>
          <a class="btn btn-primary" href="{{ route('group.edit', [ 'id' => $group->id ]) }}"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>          
          <a class="btn btn-danger" href="{{ route('group.delete', [ 'id' => $group->id ]) }}" Onclick="return ConfirmDelete();"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>&nbsp;&nbsp;
        </td>
@endif
      </tr>

    @endforeach
    </tbody>
@else 
    <p><em>There is no group yet</em></p>
@endif


</table>
</div>




@stop


<script>

function ConfirmDelete()
{
  var x = confirm("Are you sure?");
  if (x)
      return true;
  else
    return false;
}




</script>  
