@extends('layout')

@section('content')


	<h1>LIST OF PROJECT UPLOADS</h1>
	@if (Auth::user()->role=="superadmin" || Auth::user()->role=="mgr")
		<div class="new_project">
			<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>&nbsp;Project Upload</button>
		</div>
	@endif

	<!-- Modal -->
	<div id="myModal" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Project Upload</h4>
				</div>
				<div class="modal-body">
					<form id="project_form" action="{{ route('project.fupload') }}" method="POST"  enctype="multipart/form-data">
						{{ csrf_field() }}
						<label>Select Project <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span></label>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<select class="form-control" id="project_id" name="project_id">
										@if ( !$projects->isEmpty() )
											@foreach ( $projects  as $project)
												<option value="{{ $project->id }}">{{ $project->project_name }}</option>
											@endforeach
										@endif
									</select>
									<input type="hidden" name="user_id" value="{{Auth::user()->id}}">
									<input type="hidden" name="group_id" value="{{Auth::user()->group_id}}">
									<input type="hidden" name="group_name" value="{{$groupname}}">
								</div>
							</div>
						</div>

						<label>Select File to Upload <span class="glyphicon glyphicon-upload" aria-hidden="true"></span></label>
						<div class="form-group">
							<input type="file" class="form-control" name="uploaded_files" multiple required>
						</div>

						<div class="modal-footer">
							<input class="btn btn-primary" type="submit" value="Submit" >
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						</div>

					</form>
				</div>

			</div>

		</div>
	</div>
	<!--  END modal  -->



	<div class="table-responsive">
		<table class="table table-striped">
			<thead>
			<tr>
				<th>Project Name</th>
				<th>Group name</th>
				<th>Uploaded By</th>
				<th>Actions</th>
			</tr>
			</thead>


			@if ( !$fileuploads->isEmpty() )
				<tbody>
				@foreach ( $fileuploads  as $fileupload)
					<tr>
						<td>

							@if ( !$projects->isEmpty() )
								@foreach ( $projects  as $project)
									@if ( $project->id == $fileupload->project_id)
									{{ $project->project_name }}
									@endif
								@endforeach
							@endif
						</td>
						<td>{{ $groupname }} </td>
						<td>
							@if ( !$users->isEmpty() )
								@foreach ( $users  as $user)
									@if ( $user->id == $fileupload->user_id)
										{{ $user->name }}
									@endif
								@endforeach
							@endif

						</td>
						<td><a href="<?php echo '/uploads/'.$groupname."/"; ?>{{ $fileupload->uploaded_files }}">Download</a>  </td>
					</tr>

				@endforeach
				</tbody>
			@else
				<p><em>There are no tasks assigned yet</em></p>
			@endif


		</table>
	</div>




@stop


<script>

    function ConfirmDelete()
    {
        var x = confirm("Are you sure? Deleting a Project will also delete all tasks associated with this project");
        if (x)
            return true;
        else
            return false;
    }




</script>
